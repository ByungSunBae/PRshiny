# load reticulate package
library(reticulate)

# import gym module
gym <- import("gym")

# make environment : Breakout-v0
env <- gym$make("Breakout-v0")

# Random Play 5 episodes
for (i_episode in 1:5){
  
  # reset environment
  observation = env$reset()
  
  # 
  for (t_ in 1:100000L){
    # env$render() ## display game
    action <- env$action_space$sample() # random action
    tmp <- env$step(action) # observation, reward, done and info after act
    observation <- tmp[[1]]; reward <- tmp[[2]]; done <- tmp[[3]]; info <- tmp[[4]]
    print(reward)
    if (done){
      print(paste0("Episode finished after ", t_, " timesteps"))
      break
    }
  }
}
